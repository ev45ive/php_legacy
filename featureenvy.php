<?php


class OrderDetail
{
    function getAmount()
    {

    }

    function getPrice()
    {

    }

    function calculate()
    {
        return $this->getAmount() * $this->getPrice();
    }

    function getVat()
    {
        //
    }

    function hasVat()
    {
        //
    }
}

class Order
{
    public function __construct($discountPolicy)
    {
        $this->discountPolicyService = $discountPolicy;
        $this->discountPolicyService->setUser($this->getCustomer());
    }

    public function calculate()
    {
        $total = $this->calculateDetails();
        $discounted = $this->applyDiscount($total);

        return $discounted;
    }
//
//    private function getDetailBrutto($detail){
//        $subtotal = $detail->getAmount() * $detail->getPrice();
//
//        $vat = $this->getDetailVat($detail);
//        $total = $this->applyTax($subtotal, $vat->getValue());
//
//        $discounted = $this->discountPolicyService->applyDetailDiscount($this,$detail,$total);
//        return $discounted;
//    }

    function applyTax($detail)
    {

    }

    protected function calculateDetails()
    {
        $details = $this->getOrderDetails();
        $total = 0;

        foreach ($details as $detail) {
            $price = $detail->calculate();
            // apply ...

//            $discounted = $this->discountPolicyService->applyDetailDiscount($price,$detail);
//            $brutto = $this->taxPolicyService.applyTax($discounted,$detail);

            $brutto = $this->pricePolicy->calculate($price, $detail);

            $total += $brutto;

        }
        return $total;
    }


//    /**
//     *
//     * @param $detail
//     * @return mixed
//     */
//    protected function getDetailVat($detail)
//    {
//        if (!$detail->hasVat()) {
//            $vat = $this->getCustomer()->getVat();
//        } else {
//            $vat = $detail->getVat();
//        }
//        return $vat;
//    }

    private $discountPolicyService;

    /**
     * @param $isAdmin
     * @param $total
     * @return array
     */
    protected function applyDiscount($total)
    {
        $discounted = $this->discountPolicyService->applyOrderDiscount($total, $this);

        return $discounted;
    }

    private function applyTaxPercent($value, int $percent)
    {
        return $value + $this->getPercent($percent);
    }

    private function applyDiscountPercent($value, int $percent)
    {
        return $value - $this->getPercent($percent);
    }

    private function getPercent($value, $percent)
    {
        return ($value * ($percent / 100));
    }

}