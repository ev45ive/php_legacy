<?php

// java  -Dwebdriver.chrome.driver=chromedriver.exe -jar selenium-server-standalone-3.4.0.jar

namespace Specs;

use Specs\Pageobjects\ContactPage;

//class suiteTest extends \PHPUnit_Extensions_SeleniumTestSuite{
//    public function suite(){
//        $this->addTestSuite(ExampleSuite::class);
//    }
//}

class ExampleTest extends \PHPUnit_Extensions_Selenium2TestCase
{
    /**
     * @var ContactPage
     */
    private $page;

    protected function setUp()
    {
        $this->setBrowser("chrome");
        $this->setBrowserUrl("http://testhost3/");
        //shell_exec('mysql -u root contacts -e "TRUNCATE TABLE contacts;" ');
        shell_exec('mysql -u root contacts <  ' . dirname(__FILE__) . '/fixtures/contacts.sql" ');

        $this->page = new ContactPage($this);
    }

    public function testRemoveContact()
    {
        $this->url('index.php');
        $name = $this->page->getFirstName()->text();

        $this->page->getFirstRow()->byLinkText('X')
            ->click();
        $this->acceptAlert();

        $this->assertNotEquals($this->page->getFirstName()->text(), $name);

    }

    public function testAddNewContact()
    {
        $this->url('index.php');

        $this->byLinkText("New contact")
            ->click();

        $this->fillFormAndSubmit($this->correctContactProvider());

        $this->assertEquals('Testowy', $this->byLinkText("Testowy")->text());
    }

    public function correctContactProvider()
    {
        return [
            'firstname' => 'Zbigniew',
            'lastname' => 'Testowy',
            'phone' => '+324234 24234',
            'mobile' => '+324234 24234',
        ];
    }

    public function fillFormAndSubmit(array $inputs)
    {

        //    $this->byId('firstname')->value('Zbigniew');
        //    $this->assertEquals('Zbigniew',$this->byId('firstname')->value());

        $form = $this->byTag('form');
        foreach ($inputs as $input => $value) {
            $form->byName($input)->value($value);
        }
        $form->submit();
    }

    //$this->timeouts()->implicitWait(300);
//https://www.sitepoint.com/using-selenium-with-phpunit/
    private function waitForPageReload()
    {
        $webdriver = $this;
        $this->waitUntil(function () use ($webdriver) {
            try {
                $webdriver->byTag('body');

                return true;
            } catch (Exception $ex) {
                return null;
            }

        }, 2000);
    }
}

?>