-- fixtures/contacts.sql --
TRUNCATE TABLE contacts;
LOCK TABLES contacts WRITE;
INSERT INTO contacts (firstname, lastname, phone, mobile) VALUES ('Jacopo', 'Romei',
                                                                  '0543123543', '34012345');
INSERT INTO contacts (firstname, lastname, phone, mobile) VALUES ('Francesco', 'Trucchia',
                                                                  '12345', '234 12345');
UNLOCK TABLES;
