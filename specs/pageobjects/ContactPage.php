<?php
/**
 * Created by IntelliJ IDEA.
 * User: ev45i
 * Date: 8/5/2017
 * Time: 11:00
 */

namespace Specs\Pageobjects;


class ContactPage
{
    /**
     * @var \PHPUnit_Extensions_Selenium2TestCase
     */
    private $page;

    function __construct(\PHPUnit_Extensions_Selenium2TestCase $page)
    {
        $this->page = $page;
    }

    function getTable(): \PHPUnit_Extensions_Selenium2TestCase_Element
    {
        return $this->page->byCssSelector('table');
    }

    function getFirstRow(): \PHPUnit_Extensions_Selenium2TestCase_Element
    {
        return $this->getTable()->byCssSelector('tr:nth-child(2)');
    }

    function getFirstName()
    {
        return $this->getFirstRow()->byCssSelector('td:first-child a');
    }

}