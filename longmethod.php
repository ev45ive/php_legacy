<?php

class Order
{
    public function __construct($discountPolicy)
    {
        $this->discountPolicyService = $discountPolicy;
    }

    public function calculate()
    {
        $total = $this->calculateDetails();
        $discounted = $this->applyDiscount($total);

        return $discounted;
    }

    private function getDetailBrutto($detail){
        $vat = $this->getDetailVat($detail);
        $subtotal = $detail->getAmount() * $detail->getPrice();
        $total = $this->applyTax($subtotal, $vat->getValue());

        $discounted = $this->discountPolicyService->applyDetailDiscount($this,$detail,$total);
        return $discounted;
    }

    protected  function calculateDetails(){
        $details = $this->getOrderDetails();
        $total = 0;

        foreach ($details as $detail) {
            $total += $this->getDetailBrutto($detail);
        }
        return $total;
    }


    /**
     *
     * @param $detail
     * @return mixed
     */
    protected function getDetailVat($detail)
    {
        if (!$detail->hasVat()) {
            $vat = $this->getCustomer()->getVat();
        } else {
            $vat = $detail->getVat();
        }
        return $vat;
    }

    private $discountPolicyService;

    /**
     * @param $isAdmin
     * @param $total
     * @return array
     */
    protected function applyDiscount($total)
    {
        $discounted = $total;

        // Znizki (percent)
        if ($this->hasDiscount()) {
            $discounted = $this->discountPolicyService->applyOrderDiscount($this);
        }else{
            $discounted = $this->discountPolicyService->applyCustomerDiscount($this);
        }
        return $discounted;
    }

    private function applyTaxPercent($value, int $percent)
    {
        return $value + $this->getPercent($percent);
    }

    private function applyDiscountPercent($value, int $percent)
    {
        return $value - $this->getPercent($percent);
    }

    private function getPercent($value, $percent)
    {
        return ($value * ($percent / 100));
    }

}