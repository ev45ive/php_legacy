<?php
/**
 * Created by IntelliJ IDEA.
 * User: ev45i
 * Date: 8/4/2017
 * Time: 9:16
 */

interface CustomerInterface
{
    public function getFullName();
}