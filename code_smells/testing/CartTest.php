<?php
/**
 * Created by IntelliJ IDEA.
 * User: ev45i
 * Date: 8/4/2017
 * Time: 11:49
 */

namespace Testing;

use Szkolenie\Cart;
use Szkolenie\CartItem;
use PHPUnit\Framework\TestCase;
use Szkolenie\Discounts;

class CartTest extends TestCase
{
    function setUp()
    {
        parent::setUp();
        $this->cart = new Cart();
        $this->items = [
            $this->mockItem(100),
            $this->mockItem(200),
            $this->mockItem(150)
        ];
        $this->total = 450;
    }

    private function mockItem($price)
    {
        $item = $this->createMock(CartItem::class);
        $item->method('getPrice')->willReturn($price);
        return $item;
    }

    function testItemMockShouldGetPrice()
    {
        $this->assertEquals(100, $this->mockItem(100)->getPrice());
    }

    function testShouldAddItems()
    {
        $this->cart->addItem($this->items[0]);
        $this->assertEquals($this->cart->items[0], $this->items[0]);
    }

    /**
     * @depends testShouldAddItems
     */
    function testShouldCalculateTotal()
    {
        $this->cart->addItem($this->mockItem(150));
        $this->cart->addItem($this->mockItem(250));
        $total = 400;

        $this->assertEquals($total, $this->cart->getTotal());
    }


    function testShouldCheckDiscount()
    {
        $discounts = $this->getMockBuilder(Discounts::class)
            ->setMethods(['applyDiscount'])
            ->getMock();

        $this->cart->addItem($this->mockItem(100));

        $this->cart->discount = $discounts;
        $discounts->expects($this->once())
            ->method('applyDiscount')
            ->willReturn(90);

        $this->assertEquals($this->cart->getTotal(), 90);
    }

//    function testShouldCalculateDiscount(){
//
//    }

}
