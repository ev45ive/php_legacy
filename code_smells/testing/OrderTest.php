<?php
/**
 * Created by IntelliJ IDEA.
 * User: ev45i
 * Date: 8/4/2017
 * Time: 14:55
 */

namespace Testing;

use Szkolenie\Customer;
use Szkolenie\Order;
use PHPUnit\Framework\TestCase;

class OrderTest extends TestCase
{
    public function setUp()
    {
        $this->customer = new Customer('Jean Pistel');
        $this->order = new Order($this->customer);
    }

    public function testGetTotal()
    {
        $items = array(
            '34tr45' => array(
                'price' => 10,
                'description' => 'A very good CD by Jane Doe.',
                'quantity' => 2
            ),
            '34tr89' => array(
                'price' => 70,
                'description' => 'Super compilation.',
                'quantity' => 1
            )
        );
        $this->order->setItems($items);
        $this->assertEquals((20 + 70), $this->order->getTotal());
    }

    public function testGetTotalAfterRemovingItem()
    {
        $items = array(
            '34tr45' => array(
                'price' => '9.99 EUR',
                'description' => 'A very good CD by Jane Doe.',
                'quantity' => 2
            ),
            't667t4' => array(
                'price' => '69.99 EUR',

                'description' => 'Super compilation.',
                'quantity' => 1
            ),
            'jhk987' => array(
                'price' => '49.99 EUR',
                'description' => 'Foo singers. Rare edition.',
                'quantity' => 3
            ),
        );
        $this->order->setItems($items);
        unset($this->order->items['jhk987']);
        $this->assertEquals((9.99 * 2 + 69.99) . ' EUR', $this->order->getTotal());
    }

    public function testListItems()
    {
        $this->assertEquals(array(), $this->order->listItems());

        $items = array(
            '34tr45' => array(
                'price' => 10,
                'description' => 'A very good CD by Jane Doe.',
                'quantity' => 2
            ),
            '34tr89' => array(
                'price' => 70,
                'description' => 'Super compilation.',
                'quantity' => 1
            ),
        );
        $this->order->setItems($items);
        $this->assertEquals($items, $this->order->listItems());
    }

    public function testGetCustomer()
    {
        $this->order->setCustomer($this->customer);
        $this->assertEquals('Jean Pistel', $this->order->getCustomer());
    }

    public function testShippingAddress()
    {
        $this->order->setShippingAddress('84 Doe Street, London');
        $this->assertEquals('84 Doe Street, London', $this->order->getShippingAddress());
    }

    public function testDiscountForGoldSilverCustomer()
    {
        $this->assertFalse($this->order->isGoldCustomer());
        $items = array(
            '34tr45' => array(
                'price' => 9.99,
                'description' => 'A very good CD by Jane Doe.',
                'quantity' => 2
            ),
            '34tr89' => array(
                'price' => 69.99,
                'description' => 'Super compilation.',
                'quantity' => 1
            ),
        );
        $this->order->setItems($items);
        $this->assertEquals((19.98 + 69.99), $this->order->getTotal());
        $this->customer->makeSilver();
        $this->assertEquals(71.98, $this->order->getTotal());
        $this->customer->makeGold();
        $this->assertEquals(53.98, $this->order->getTotal());
    }

    public function testDiscountOverOrderTotal()
    {
        $items = array(
            '34tr45' => array(
                'price' => 300,
                'description' => 'A very good CD by Jane Doe.',
                'quantity' => 1
            ),
            '34tr89' => array(
                'price' => 270,
                'description' => 'Super compilation.',
                'quantity' => 1
            ),
        );
        $this->order->setItems($items);
        $this->assertEquals(570 * 0.9, $this->order->getTotal());
    }
}
