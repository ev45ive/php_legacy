<?php
/**
 * Created by IntelliJ IDEA.
 * User: ev45i
 * Date: 8/4/2017
 * Time: 10:11
 */

namespace Testing;

use PHPUnit\Framework\TestCase;
use \Szkolenie\CartItem;

class CartItemTest extends TestCase
{
    public function testShouldCalculateItemPrice()
    {
        $sale = new CartItem();
        $sale->amount = 2;
        $sale->value = 23;

        $this->assertEquals(46, $sale->getPrice());
    }

    public function testShouldErrorOnWrongAmount()
    {
        $sale = new CartItem();
        $sale->amount = -2;
        $sale->value = 23;

        $this->expectException(\Exception::class);
        $sale->getPrice();
    }
}
