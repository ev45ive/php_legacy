<?php
/**
 * Created by IntelliJ IDEA.
 * User: ev45i
 * Date: 8/4/2017
 * Time: 15:54
 */

namespace Szkolenie;


class GoldAffilation extends Affiliation
{
    function calculateDiscount($total)
    {
        // If the customer is gold we apply 40% discount and...
        $total = $total * 0.6;
        // ...if amount is over 500 we apply further 20% discount
        if ($total > 500) {
            $total = $total * 0.8;
        }
        return $total;
    }
}