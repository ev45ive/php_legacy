<?php
/**
 * Created by IntelliJ IDEA.
 * User: ev45i
 * Date: 8/4/2017
 * Time: 15:15
 */

namespace Szkolenie;

class Customer
{

    const GOLD = 'gold';
    const SILVER = 'silver';

    private $type;
    private $affiliation;

    // Customer
    protected $first_name;
    protected $last_name;
    protected $customer_address;
    protected $customer_city;
    protected $customer_country;

    function __construct($customer)
    {
        list($this->first_name, $this->last_name) = explode(' ', $customer);
        $this->affiliation = new DefaultAffiliation();
    }

    function isSilver()
    {
        return $this->type == self::SILVER;
    }

    function isGold()
    {
        return $this->type == self::GOLD;
    }

    function makeSilver()
    {
        $this->affiliation = new SilverAffiliation();
        return $this->type = self::SILVER;
    }

    function makeGold()
    {
        $this->affiliation = new GoldAffilation();
        return $this->type = self::GOLD;
    }

    function __toString()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * @return Affiliation
     */
    public function getAffiliation(): Affiliation
    {
        return $this->affiliation;
    }
}