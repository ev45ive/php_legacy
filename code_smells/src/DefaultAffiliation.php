<?php
/**
 * Created by IntelliJ IDEA.
 * User: ev45i
 * Date: 8/4/2017
 * Time: 15:53
 */

namespace Szkolenie;


class DefaultAffiliation extends Affiliation
{
    function calculateDiscount($total)
    {
        // if customer subscribed no fidelity program we apply 10% over 500
        if ($total > 500) {
            $total = $total * 0.9;
        }
        return $total;
    }

}