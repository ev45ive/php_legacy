<?php
/**
 * Created by IntelliJ IDEA.
 * User: ev45i
 * Date: 8/4/2017
 * Time: 10:28
 */

namespace Szkolenie;

class CartItem
{
    public
        $amount,
        $value;

    function getPrice()
    {
        if ($this->amount < 0) {
            throw new \Exception('Amount cannot be negative');
        }

        return $this->amount * $this->value;
    }

}