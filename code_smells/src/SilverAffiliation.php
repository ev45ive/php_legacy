<?php
/**
 * Created by IntelliJ IDEA.
 * User: ev45i
 * Date: 8/4/2017
 * Time: 15:53
 */

namespace Szkolenie;


class SilverAffiliation extends Affiliation
{
    function calculateDiscount($total)
    {
        // If the customer is silver we apply 20% discount and...
        $total = $total * 0.8;
        // ...if amount is over 500 we apply further 10% discount
        if ($total > 500) {
            $total = $total * 0.9;
        }
        return $total;
    }
}