<?php

namespace Szkolenie;

class Order
{
    public $items = array();

    protected $customer;

    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }

    // Shipping
    protected $shipping_address;

    public function setItem($code, $price, $description, $quantity)
    {
        $this->items[] = array(
            'code' => $code,
            'price' => $price,
            'description' => $description,
            'quantity' => $quantity
        );
    }

    public function setItems($items)
    {
        $this->items = $items;
    }

    public function listItems()
    {
        return $this->items;
    }

    public function setCustomer($customer)
    {
        $this->customer = new Customer($customer);
    }

    public function getCustomer()
    {
        return $this->customer;
    }

    public function setShippingAddress($address)
    {
        $this->shipping_address = $address;
    }

    public function getShippingAddress()
    {
        return $this->shipping_address;
    }

    public function isGoldCustomer()
    {
        return $this->customer->isGold();
    }

    public function getTotal()
    {
        $total = 0;
        foreach ($this->items as $item) {
            $currency = '';

            // we check for the item to be valid
            if (isset($item['price']) && isset($item['quantity'])) {
                // we detect currency if indicated
                $price = explode(' ', $item['price']);
                if (isset($price[1])) {
                    $currency = $price[1];
                }
                $price = $price[0];
                $total += $price * $item['quantity'];
            }
        }
        $total = $this->getCustomer()
            ->getAffiliation()
            ->calculateDiscount($total);

//        $total = $this->getPromotions()
//            ->calculateDiscount($total);

        if ($currency) {
            return round($total, 2) . ' ' . $currency;
        } else return round($total, 2);
    }
}
