<?php
/**
 * Created by IntelliJ IDEA.
 * User: ev45i
 * Date: 8/4/2017
 * Time: 11:49
 */

namespace Szkolenie;


class Cart
{
    public
        /**
         * @var CartItem[]
         */
        $items = [],
        /**
         * @var Discounts
         */
        $discount;

    public function addItem(CartItem $item)
    {
        $this->items[] = $item;
    }

    public function getTotal()
    {
        $total = 0;
        foreach ($this->items as $item) {
            $total += $item->getPrice();
        }

        if (isset($this->discount)) {
            return $this->discount->applyDiscount($total);
        } else {
            return $total;
        }
    }
}