<?php
/**
 * Created by IntelliJ IDEA.
 * User: ev45i
 * Date: 8/4/2017
 * Time: 12:44
 */

namespace Szkolenie;


class Discounts
{
    function calculateDiscount()
    {
        return 5;
    }

    function applyDiscount($amount)
    {
        $discount = $this->calculateDiscount();
        return $amount * (1 + ($discount / 100));
    }

}