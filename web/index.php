<?php

//ini_set('display_errors', 0);

require_once __DIR__ . '/../vendor/autoload.php';

use \App\App;

$app = new App();

$app->run();