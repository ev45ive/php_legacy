<?php
/**
 * Created by IntelliJ IDEA.
 * User: ev45i
 * Date: 8/3/2017
 * Time: 14:47
 */

interface RepositoryInterface
{
    public function __construct(Connection $connection);

    public function retrieveAll();

    public function retrieveOneById($id);
}