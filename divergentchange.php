<?php
require('RepositoryInterface.php');


class Connection
{

    protected $connection;

    public function __construct($config)
    {
        $this->connection = new mysqli("example.com", "user", "password", "database");
    }

    public function prepare($sql)
    {
        return $this->connection->prepare($sql);
    }

    public function execute($stmt)
    {
        return $stmt->execute();
    }
}

abstract class AbstractRepository implements RepositoryInterface
{

    protected $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }
}

class OrderRepository extends AbstractRepository
{

    public function retrieveAll()
    {
        $query = $this->connection->prepare('SELECT * FROM order');
        return $query->execute();
    }

    public function retrieveOneById($id)
    {
        $query = $this->connection->prepare('SELECT * FROM orde where id = ? ');
        $query->bind_param('id', $id);
        return $query->execute();
    }
}


class CustomerRepository extends AbstractRepository
{


    public function retrieveAll()
    {
        // TODO: Implement retrieveAll() method.
    }

    public function retrieveOneById($id)
    {
        // TODO: Implement retrieveOneById() method.
    }
}