<?php


interface CustomerInterface
{
    public function getFullName();
}

class Company implements CustomerInterface
{

    protected $company_name;

    public function getFullName()
    {
        return $this->getCompanyName();
    }
}

class Customer implements CustomerInterface
{
    protected $first_name;
    protected $last_name;

    public function getFullName()
    {
        // NULL OBJECT:
//        if ($this->hasDiscount) {
        $this->discount = NoDiscount();
        return $this->getApplyDiscount();
        //      }

        return $this->getFirstName() . ' ' . $this->getLastName();
    }
}