<?php
include_once('config.php');
if (!$_GET['id']) {
    die('Some error occured!!');
}

$connection = @mysqli_connect($database['host'], $database['username'], $database['password']) or
die('Can\'t connect to database');
$db = @mysqli_select_db($connection, $database['name']) or die('The database selected does not exists');

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $errors = validate(array('id', 'firstname', 'lastname', 'phone'), $_POST);
    if (count($errors) == 0) {
        $query = sprintf("UPDATE contacts set firstname = '%s',
 lastname = '%s',
 phone = '%s',
 mobile = '%s' WHERE id = %s",
            mysqli_real_escape_string($connection, $_POST['firstname']),
            mysqli_real_escape_string($connection, $_POST['lastname']),
            mysqli_real_escape_string($connection, $_POST['phone']),
            mysqli_real_escape_string($connection, $_POST['mobile']),
            mysqli_real_escape_string($connection, $_POST['id'])
        );
        $rs = mysqli_query($connection, $query);
        if (!$rs) {
            die_with_error(mysqli_error($connection), $query);
        }
        header('Location: index.php');
    }
} else {
    $query = sprintf('SELECT * FROM contacts WHERE id = %s', mysqli_real_escape_string($connection, $_GET['id']));
    $rs = mysqli_query($connection, $query);
    if (!$rs) {
        die_with_error(mysqli_error($connection), $query);
    }
    $row = mysqli_fetch_assoc($rs);
    $_POST['id'] = $row['id'];
    $_POST['firstname'] = $row['firstname'];
    $_POST['lastname'] = $row['lastname'];
    $_POST['phone'] = $row['phone'];
    $_POST['mobile'] = $row['mobile'];
}
mysqli_close($connection);
?>
<?php include_once('header.php') ?>
<?php include_once('_form.php') ?>
<?php include_once('footer.php') ?>