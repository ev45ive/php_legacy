<?php
/**
 * Validate form
 *
 * @param array $mandatory_fields
 * @param array $fields
 * @return array
 */
function validate($mandatory_fields, $fields)
{
    $errors = array();

    foreach ($mandatory_fields as $field) {
        if ($fields[$field] == '') {
            $errors[] = 'The ' . $field . ' field is mandatory';
        }
    }

    return $errors;
}

function die_with_error($error_msg, $query)
{
    $message = 'Invalid query: ' . $error_msg . "\n";
    $message .= 'Whole query: ' . $query;
    die($message);
}

?>