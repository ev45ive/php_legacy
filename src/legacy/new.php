<?php
include_once('config.php');
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $errors = validate(array('firstname', 'lastname', 'phone'), $_POST);
    if (count($errors) == 0) {
        $connection = @mysqli_connect($database['host'], $database['username'], $database['password']) or
        die('Can\'t connect to database');
        $db = @mysqli_select_db($connection, $database['name']) or die('The database selected does not exists');
        $query = sprintf("INSERT INTO contacts (firstname, lastname, phone, mobile) VALUES ('%s',
'%s', '%s', '%s')",
            mysqli_real_escape_string($connection, $_POST['firstname']),
            mysqli_real_escape_string($connection, $_POST['lastname']),
            mysqli_real_escape_string($connection, $_POST['phone']),
            mysqli_real_escape_string($connection, $_POST['mobile'])
        );
        // $contact = new Contact();
        // $this->ContactRepository->save($contact)
        $rs = mysqli_query($connection, $query);
        if (!$rs) {
            die_with_error(mysqli_error($connection), $query);
        }
        mysqli_close($connection);
        header('Location: index.php');
    }
}
?>
<?php include_once('header.php') ?>
<?php include_once('_form.php') ?>
<?php include_once('footer.php') ?>
