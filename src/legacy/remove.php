<?php
include_once('config.php');
if (!$_GET['id']) {
    die('Some error occured!!');
}

$connection = @mysqli_connect($database['host'], $database['username'], $database['password']) or
die('Can\'t connect to database');
$db = @mysqli_select_db($connection, $database['name']) or die('The database selected does not exists');

$query = sprintf('DELETE FROM contacts where ID = %s',
    mysqli_real_escape_string($connection, $_GET['id']));
if (!mysqli_query($connection, $query)) {
    die_with_error(mysqli_error($connection), $query);
}
mysqli_close($connection);
header('Location: index.php');
?>