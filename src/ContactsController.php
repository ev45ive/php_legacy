<?php
/**
 * Created by IntelliJ IDEA.
 * User: ev45i
 * Date: 8/5/2017
 * Time: 13:54
 */

namespace App;


class ContactsController
{
    private $connection;

    function __construct()
    {
        include_once(__DIR__ . '/legacy/config.php');
        $this->connection = @mysqli_connect($database['host'], $database['username'], $database['password']) or
        die('Can\'t connect to database');
        $db = @mysqli_select_db($this->connection, $database['name']) or die('The database selected does not exists');
    }

    function indexAction()
    {


        $query = 'SELECT * FROM contacts ORDER BY lastname';
        $rs = mysqli_query($this->connection, $query);
        if (!$rs) {
            die_with_error(mysqli_error($this->connection), $query);
        }

        $results = mysqli_fetch_all($rs, MYSQLI_ASSOC);

        $context = [
            'num' => mysqli_num_rows($rs),
            'results' => $results
        ];

        $response = $this->renderIndex($context);
        // $this->renderer->render('index.tempalte',$context);

        mysqli_free_result($rs);
        mysqli_close($this->connection);

        return $response;
    }

    function renderIndex($context)
    {

        $num = $context['num'];
        $results = $context['results'];

        ob_start();
        include_once(__DIR__ . '/legacy/header.php')
        ?>
        <div class="actions">
            <a href="/new.php">New contact</a>
        </div>

        <?php if ($num) : ?>
        <table border="1" cellspacing="0" cellpadding="5">
            <tr>
                <th>Last Name</th>
                <th>First Name</th>
                <th>Phone</th>
                <th>Mobile</th>
                <th>&nbsp;</th>
            </tr>
            <?php foreach ($results as $row) : ?>
                <tr>
                    <td><a href="/edit.php?id=<?php echo $row['id'] ?>" title="Edit"><?php echo
                            $row['lastname'] ?></a></td>
                    <td><?php echo $row['firstname'] ?></a></td>
                    <td><a href="callto://<?php echo $row['phone'] ?>"><?php echo $row['phone'] ?></a></td>
                    <td><a href="callto://<?php echo $row['mobile'] ?>"><?php echo $row['mobile'] ?></a></td>
                    <td>[<a href="/remove.php?id=<?php echo $row['id'] ?>" title="Delete" onclick="if
(confirm('Are you sure?')) {return true;} return false;">X</a>]
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    <?php else : ?>
        Database is empty
    <?php endif ?>
        <?php include_once(__DIR__ . '/legacy/footer.php') ?>
        <?php

        return ob_get_clean();
    }

}