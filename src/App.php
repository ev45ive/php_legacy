<?php

namespace App;

class App
{

    function run()
    {
        switch ($_SERVER['PATH_INFO']) {
            case '/new.php':
                require __DIR__ . '/legacy/new.php';
                break;
            case '/remove.php':
                require __DIR__ . '/legacy/remove.php';
                break;
            default:
            case '/index.php':
                //require __DIR__.'/legacy/index.php';
                $controller = new ContactsController();
                echo $controller->indexAction();
                break;
        }
    }

}